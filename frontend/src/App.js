import React, { Component } from "react";
import Listings from "./components/Listings";
import Searchbar from "./components/Searchbar";
import Register from "./components/Register";
import Profile from "./components/Profile";
import Search from "./components/Search";
import Verify from "./components/Verify";
import VerifyTH from "./components/VerifyTH";
import Host_rooms from "./components/Host_rooms";
import Filters from "./components/Filters";
import Add_room from "./components/Add_room";
import View_room from "./components/View_room";
import View_bookings from "./components/View_bookings";
import Recommendations from "./components/Recommendations";
import Update_room from "./components/Update_room";
import More_filters from "./components/More_filters";
import { Input, Button } from "@material-ui/core";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import axios from "axios";
import Cookie from "js-cookie";
import jwt_decode from 'jwt-decode';

var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: "",
			password: "",
		};
	}

	render() {
		
		return (
			<Router>
				<div
					className="App"
					style={{
						height: "100vh",
						backgroundImage:
							"url(" +
							"https://free4kwallpapers.com/uploads/originals/2020/08/15/dubai-united-arab-emirates.-wallpaper_.jpg" +
							")",
						backgroundPosition: "center",
						backgroundSize: "cover",
						backgroundRepeat: "no-repeat",
						backgroundAttachment: "fixed",
						overflowX: "scroll",
					}}
				>
					<div
						className="wrapbar"
						style={{
							backgroundColor: "#b3b3b3",
							position: "sticky",
							top: "0",
							zIndex: "1",
						}}
					>
						<h1
							style={{
								float: "left",
								paddingLeft: "1rem",
								margin: "auto",
							}}
						>
							<Link to="/" style={{ textDecoration: 'none',textTransform: 'capitalize' }}>Airbnb</Link>
						</h1>
						<div
							style={{
								display: "flex",
								justifyContent: "flex-end",
							}}
						>
							<div
								style={{
									padding: "0.5rem",
								}}
							></div>
							<div
								style={{
									paddingRight: "1rem",
									paddingLeft: "1rem",
								}}
							>
								{!Cookie.get("token") ? (
									
									<div
										className="searchbar"
										style={{
											float: "right",
											display: "flex",
										}}
									>
										
										<div
											style={{
												padding: "0.5rem",
											}}
										>
											<Input
												className="searchbar"
												type="text"
												label="Email"
												placeholder="Email"
												s={6}
												style={{
													paddingRight: "2rem",
												}}
												onChange={(event) => {
													this.setState({
														email:
															event.target.value,
													});
												}}
											/>

											<Input
												className="searchbar"
												type="password"
												label="Password"
												placeholder="Password"
												s={6}
												onChange={(event) => {
													this.setState({
														password:
															event.target.value,
													});
												}}
											/>
											<Button
												variant="outlined"
												color="secondary"
												onClick={() => {
													if (
														this.state.email &&
														this.state.password
													) {
														const user = {
															email: this.state
																.email,
															password: this.state
																.password,
														};
														axios
															.post(
																"https://localhost:3000/user/login",
																user
															)
															.then(
																(response) => {
																	if (response.data.message === "Auth successful"	) 
																	{
																		Cookie.set(
																			"token",
																			response
																				.data
																				.token
																		);
																		window.location.reload();
																		
																	
																	}
																	else{
																		alert(response.data.message);
																	}
																}
															)
															.catch((error) => {
																console.log(
																	error.response
																);
															});
													}
												}}
											>
												Login
											</Button>
											<Button
												variant="outlined"
												color="secondary"
												style={{
													paddingLeft: "1rem",
												}}
											>
												<Link to="/register"style={{ textDecoration: 'none',textTransform: 'capitalize' }}>
													Register
												</Link>
											</Button>
										</div>
									</div>
								) : (

									
									<>
										

									<Button>
									<Link to="/profile"style={{ textDecoration: 'none',textTransform: 'capitalize' }}>
									Profile Settings
									</Link>
									</Button>


									<Button
										className="searchbar"
										style={{ paddingRight: "1rem" }}
										onClick={() => {
											Cookie.remove("token");
											window.location.reload();
										}}
									>
									<Link to="/" style={{ textDecoration: 'none',textTransform: 'capitalize' }}>
									Logout
									</Link>
									</Button>
									</>
								)}
							</div>
						</div>
					</div>
					<div>
						<hr />
						<Switch>
							<Route exact path="/" component={Searchbar} />
							<Route exact path="/register" component={Register}/>
							<Route exact path="/rooms" component={Listings} />
							<Route exact path="/result" component={Search} />
							<Route exact path="/profile" component={Profile} />
							<Route exact path="/verify" component={Verify} />
							<Route exact path="/add_room" component={Add_room} />
							<Route exact path="/view_bookings" component={View_bookings} />
							<Route exact path="/filters" component={Filters} />
							<Route exact path="/rooms/:id" component={View_room} />
							<Route exact path="/more_filters" component={More_filters} />
							<Route exact path="/host_rooms" component={Host_rooms} />
							<Route exact path="/recommendations" component={Recommendations} />
							<Route exact path="/verifytendershosts" component={VerifyTH} />
							<Route exact path="/updateroom/:id" component={Update_room} /> 

							
						</Switch>
					</div>

				</div>
			</Router>
		);
	}
}

export default App;
