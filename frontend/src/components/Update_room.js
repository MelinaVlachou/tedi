import "../App.css";
import Cookie from "js-cookie";
import React, { Component } from "react";
import { Input, Button } from "@material-ui/core";
import axios from "axios";
import jwt_decode from 'jwt-decode';


var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}


export default class Update_room extends Component {
    constructor(props) {
		super(props);
		this.state = { 
            
			name: "" ,
			price: "" ,
			no_people: "" ,
			location: "" ,
			type: "" ,
			sqmeters: "" ,
			floor: "" ,
			min_no_rentingdays: "" ,

		};
        
    }
    
render() {

	return (
	<div>
		{!Cookie.get("token")?
		(<><h2 > You are not authorized to view this page</h2> </>):
		(<>
		<h1 style={{ fontSize: "3rem" }}>
			<p className="shadow-box-example hoverable">Update room's info</p>
		</h1>
		
		
		<Input
		
			style={{
				
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
            label="Name"
            placeholder={"Room name"}
			onChange={(event) => {
				this.setState({
					name: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var rooms = [
					{
						"propName" :"name" , "value" : this.state.name,
					}];
				
				const roomId = this.props.match.params.id;
				console.log(JSON.stringify(rooms));
				axios
					.patch(
						"https://localhost:3000/rooms/"+ roomId,
						JSON.parse(
							JSON.stringify(rooms)
						)
					)
					.then((response) => {
                        alert(response.data.message);
                        window.location.reload();

					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>

		<br />
		<br />
		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
            label="Price"
            placeholder={"Price per day"}
			onChange={(event) => {
				this.setState({
					price: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var rooms = [
					{
						"propName" :"price" , "value" : this.state.price,
					}];
				
				const roomId = this.props.match.params.id;
				console.log(JSON.stringify(rooms));
				axios
					.patch(
						"https://localhost:3000/rooms/"+ roomId,
						JSON.parse(
							JSON.stringify(rooms)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
            label="Occupants"
            placeholder={"Occupants"}
			onChange={(event) => {
				this.setState({
					no_people: event.target.value,
				});
			}}
			
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var rooms = [
					{
						"propName" :"no_people" , "value" : this.state.no_people,
					}];
				
				const roomId = this.props.match.params.id;
				console.log(JSON.stringify(rooms));
				axios
					.patch(
						"https://localhost:3000/rooms/"+ roomId,
						JSON.parse(
							JSON.stringify(rooms)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
            label="Location"
            placeholder={"Location"}
			onChange={(event) => {
				this.setState({
					location: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var rooms = [
					{
						"propName" :"location" , "value" : this.state.location,
					}];
				
				const roomId = this.props.match.params.id;
				console.log(JSON.stringify(rooms));
				axios
					.patch(
						"https://localhost:3000/rooms/"+ roomId,
						JSON.parse(
							JSON.stringify(rooms)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		

		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
            label="Type"
            placeholder={"Type"}
			onChange={(event) => {
				this.setState({
					type: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var rooms = [
					{
						"propName" :"type" , "value" : this.state.type,
					}];
				
				const roomId = this.props.match.params.id;
				console.log(JSON.stringify(rooms));
				axios
					.patch(
						"https://localhost:3000/rooms/"+ roomId,
						JSON.parse(
							JSON.stringify(rooms)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
            label="Sqmeters"
            placeholder={"Sqmeters"}
			onChange={(event) => {
				this.setState({
					sqmeters: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var rooms = [
					{
						"propName" :"sqmeters" , "value" : this.state.sqmeters,
					}];
				
				const roomId = this.props.match.params.id;
				console.log(JSON.stringify(rooms));
				axios
					.patch(
						"https://localhost:3000/rooms/"+ roomId,
						JSON.parse(
							JSON.stringify(rooms)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />

		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
            label="floor"
            placeholder={"Floor"}
			onChange={(event) => {
				this.setState({
					floor: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var rooms = [
					{
						"propName" :"floor" , "value" : this.state.floor,
					}];
				
				const roomId = this.props.match.params.id;
				console.log(JSON.stringify(rooms));
				axios
					.patch(
						"https://localhost:3000/rooms/"+ roomId,
						JSON.parse(
							JSON.stringify(rooms)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />

		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
            label="minrent"
            placeholder={"Minimum renting"}
			onChange={(event) => {
				this.setState({
					min_no_rentingdays: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var rooms = [
					{
						"propName" :"min_no_rentingdays" , "value" : this.state.min_no_rentingdays,
					}];
				
				const roomId = this.props.match.params.id;
				console.log(JSON.stringify(rooms));
				axios
					.patch(
						"https://localhost:3000/rooms/"+ roomId,
						JSON.parse(
							JSON.stringify(rooms)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		</>)}
	</div>
	
	
	);
	
}

}
