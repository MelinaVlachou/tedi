import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import "../App.css";
import axios from "axios";

export default class Search extends Component {
	constructor(props) {
		super(props);

		this.state = {
			rooms: [],
		};
	}

	componentDidMount = () => {
		if (this.props.location.searchProp && this.props.location.searchProp) {
			const query = {
				room: {
                    price : this.props.location.searchProp.price,
                    living_room:  this.props.location.searchProp.living_room,
                    smoking : this.props.location.searchProp.smoking,
                    pets : this.props.location.searchProp.pets,
                    gatherings : this.props.location.searchProp.gatherings,
                    min_no_rentingdays : this.props.location.searchProp.min_no_rentingdays,
                    host_name : this.props.location.searchProp.host_name,
                    kitchen : this.props.location.searchProp.kitchen,
                    no_bathrooms : this.props.location.searchProp.no_bathrooms,
                    no_beds : this.props.location.searchProp.no_beds,
                    no_bedrooms : this.props.location.searchProp.no_bedrooms,
                    type : this.props.location.searchProp.type,
                    sqmeters : this.props.location.searchProp.sqmeters,
                    heating : this.props.location.searchProp.heating,
                    floor : this.props.location.searchProp.floor,
                    wifi : this.props.location.searchProp.wifi,
                    television : this.props.location.searchProp.television,
                    air_conditioning: this.props.location.searchProp.air_conditioning,
                    parking : this.props.location.searchProp.parking,
                    name : this.props.location.searchProp.name,
				},
				start_date: this.props.location.searchProp.start_date,
				end_date: this.props.location.searchProp.end_date,
			};
			if (this.props.location.searchProp.location) {
				query.room.location = this.props.location.searchProp.location;
			}
			if (this.props.location.searchProp.no_people) {
                query.room.no_people = this.props.location.searchProp.no_people;
			}
			console.log(query);

			axios
				.post(`https://localhost:3000/rooms/search`, query)
				.then((res) => {
					const listings = res.data.rooms;
					this.setState({ rooms: listings });
				})
				.catch((error) => {
					console.log(error);
					alert(error.message);
				});
		}
	};

	render() {
		let markersListings = this.state.rooms.map((data, index) => (
			<div
				style={{ width: "80%", margin: "auto", marginBottom: "2rem" }}
				key={index}
			>
				<Card style={{ maxWidth: "345" }}>
					<CardActionArea>
						<CardMedia
							component="img"
							alt="Apartment image"
							height="345"
							image="logo512.png"
							title="Apartment image"
						/>
						<CardContent>
							<Typography
								gutterBottom
								variant="h5"
								component="h2"
							>
								{data.name}
							</Typography>
							<Typography
								gutterBottom
								variant="h6"
								component="h2"
							>
								{data.location} - {data.price}€
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{data.description}
							</Typography>
						</CardContent>
					</CardActionArea>
					<CardActions>
						<Button size="small" color="primary">
							Learn More
						</Button>
					</CardActions>
				</Card>
			</div>
		));

		return <div className="row">{markersListings}</div>;
	}
}