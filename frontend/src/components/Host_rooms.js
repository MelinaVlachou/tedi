import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import "../App.css";
import { Link } from "react-router-dom";
import axios from "axios";
import Cookie from "js-cookie";
import jwt_decode from 'jwt-decode';

var token =Cookie.get("token");

if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}

export default class Host_rooms extends Component {
	constructor(props) {
		super(props);

		this.state = {
			rooms: [],
			
		};
	}

	componentDidMount = () => {
		if (1) {
			const query = {  
            	host_name : decoded.name,      	
			};
			
			console.log(query);

			axios
				.post(`https://localhost:3000/rooms/host_name`, query)
				.then((res) => {
					const listings = res.data.room;
					this.setState({ rooms: listings });
				})
				.catch((error) => {
					console.log(error);
					alert(error.message);
				});
		}
	};

	render() {

		let markersListings = this.state.rooms.map((data, index) => (
			<div
				style={{ width: "20%", margin: "auto", marginBottom: "1rem" }}
				key={index}
			>
				{!Cookie.get("token")?(<>
			<h2 >
				You are not authorized to view this page
			</h2> </>):(<>
				{ !(decoded.role==="host" || decoded.role==="tenderhost" ) ?
				(<h2 >	You are not authorized to view this page </h2>) 
				: (
				<>
				<Card style={{ maxWidth: "145" }}>
					<CardActionArea>
						<CardContent>
							<Typography
								gutterBottom
								variant="h10"
								component="h2"
							>
								{"Name: "}
								{data.name}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{"Host name: "}
								{ data.host_name}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{"Type of room: "}
								{data.type}
							</Typography>
						</CardContent>
					</CardActionArea>
					<CardActions>
						<Button size="small" variant="outlined" color="secondary"
							onClick={() => {
								
								 
									 const roomId=data._id;
								
								axios
									.delete(
										"https://localhost:3000/rooms/"+ roomId
										
									)
									.then((response) => {
										console.log(response);
										window.location.reload();
									})
									.catch((error) => {
										console.log(error.response);
									});
								
							}}
						>
							Delete
							
						</Button> 
						<br></br>
						<br></br>
						<Button size="small" variant="outlined" color="secondary">
						<Link  
						to={{
							pathname: `/updateroom/${data._id}`,
							idprop: data._id,
							nameprop: data.name,
						}}
						style={{ textDecoration: 'none',textTransform: 'capitalize' }}>Update info</Link>	
						</Button> 
					</CardActions>
				</Card>
				</>)}
				</>)}
			</div>
		));

		return <div className="row">{markersListings}</div>;
	}
    
}