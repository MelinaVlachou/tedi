import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import "../App.css";
import axios from "axios";
import { Link } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import Cookie from "js-cookie";
import jwt_decode from 'jwt-decode';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'


var token =Cookie.get("token");

if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}
	const position = [38.0550, 23.8077];


export default class View_room extends Component {
	constructor(props) {
		super(props);
		var today = new Date(),
		date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	
		this.state = {
			room: null,
			currentDate: date,
			start_date: "",
			end_date: "",

		};
		
	}
	componentWillMount = () => {
        axios.get(`https://localhost:3000/rooms/`+this.props.match.params.id)
        .then((res) => {
            const listings = res.data.room;
			this.setState({ room: listings });
			console.log(this.state.room);
		});
	};
	

	render() {
		
			return <div
				style={{ width: "80%", margin: "auto", marginBottom: "2rem" }}
				
			>
				<Map center={position} zoom={13}>
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                
              />
              <Marker position={position}>
              <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.1/leaflet.css"></link>
                <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
              </Marker>
            </Map> 
				{this.state.room?
				<Card style={{ maxWidth: "345" }}>
					<CardActionArea>			
						
						<CardContent>
					
							<Typography
								gutterBottom
								variant="h5"
								component="h2"
							>
								General information for {this.state.room.name?this.state.room.name:""}
							</Typography>
							<Typography
								gutterBottom
								variant="h6"
								component="h2"
							>
								Location: {this.state.room.location?this.state.room.location:""} 
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Price per day: {this.state.room.price?this.state.room.price:""}€
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Type of room: {this.state.room.type?this.state.room.type:""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Room capacity: {this.state.room.no_people?this.state.room.no_people:""} 
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Floor: {this.state.room.floor?this.state.room.floor:""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Square Meters: {this.state.room.sqmeters?this.state.room.sqmeters:""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Provides Heating: {this.state.room.heating?this.state.room.heating.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Provides air conditioning: {this.state.room.air_conditioning?this.state.room.air_conditioning.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Provides wifi: {this.state.room.wifi?this.state.room.wifi.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Provides TV: {this.state.room.television?this.state.room.television.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Bathrooms: {this.state.room.no_bathrooms?this.state.room.no_bathrooms:""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Beds: {this.state.room.no_beds?this.state.room.no_beds:""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Bedrooms: {this.state.room.no_bedrooms?this.state.room.no_bedrooms:""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Includes livingroom: {this.state.room.living_room?this.state.room.living_room.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Includes kitchen: {this.state.room.kitchen?this.state.room.kitchen.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Description: {this.state.room.description?this.state.room.description:""}
							</Typography>


							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Elevator: {this.state.room.elevator?this.state.room.elevator.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Parking: {this.state.room.parking?this.state.room.parking.toString():""}
							</Typography>
							
							
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Smoking allowed: {this.state.room.smoking?this.state.room.smoking.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Pets allowed: {this.state.room.pets?this.state.room.pets.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Gatherings allowed: {this.state.room.gatherings?this.state.room.gatherings.toString():""}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								Minimum number of renting days: {this.state.room.min_no_rentingdays?this.state.room.min_no_rentingdays:""}
							</Typography>
							<div style={{float: 'right'}}>
							
							{/* IF YOU ARE ANONYMOUS */}
							{!Cookie.get("token")?(<>
							<h3 >
								You must login in order to make a reservation
							</h3> </>):(<>

							<Typography
							variant="body2"
								color="textSecondary"
								component="p"
							>
								<form noValidate>
							<TextField
							style={{
								backgroundColor: "white",
								borderRadius: 5
							}}
							id="date"
							label="From"
							type="date"
							defaultValue={this.state.currentDate}
							InputLabelProps={{
								shrink: true,
							}}
							onChange={(event) => {
								this.setState({
									start_date: event.target.value,
								});
							}}
						/>
					</form>
							<form noValidate>
							<TextField
							style={{
								backgroundColor: "white",
								borderRadius: 5
							}}
							id="date"
							label="To"
							type="date"
							defaultValue={this.state.currentDate}
							InputLabelProps={{
								shrink: true,
							}}
							onChange={(event) => {
								this.setState({
									end_date: event.target.value,
								});
							}}
						/>
							</form>
					
					<br></br>
					<CardActions>
					<Button variant="outlined" color="secondary"
					onClick={() => {
						if (1) {
							const booking = {
								room: this.props.match.params.id,
								end_date : this.state.end_date,
								start_date : this.state.start_date,
								reservation_name: decoded.name,
								tel : decoded.phone,
								room_name : this.state.room.name,
								
							};
							axios
								.post(
									"https://localhost:3000/bookings/",
									booking
								)
								.then((response) => {
									alert(response.data.message);
								})
								.catch((error) => {
									alert(error.response.data.message);
								});
						}
					}}
					>
						<Link to="/" style={{ textDecoration: 'none',textTransform: 'capitalize' }}>Make reservation</Link>
					
					</Button>
					</CardActions>
					</Typography>
					</>)}
					</div>
					<div style={{clear: 'both'}}></div>



						</CardContent>
					</CardActionArea>
					
				</Card>
				:""}
			</div>
		
        
	}
}

