import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import "../App.css";
import axios from "axios";
import Cookie from "js-cookie";
import jwt_decode from 'jwt-decode';
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";

var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}

export default class View_bookings extends Component {
	constructor(props) {
		super(props);
	
		this.state = {
			bookings: [],
			rating: "",
			room: "",
			user: "",
	
		};
	}
	
	componentDidMount = () => {
        axios.get(`https://localhost:3000/bookings/username/`+ decoded.name)
        .then((res) => {
            const listings = res.data.bookings;
			this.setState({ bookings: listings });
			
		});
		
	};
	
    
	render() {
		let markersListings = this.state.bookings.map((data, index) => (
			<div
				style={{ width: "80%", margin: "auto", marginBottom: "2rem" }}
				key={index}
			>
				
				<Card style={{ maxWidth: "345" }}>
					<CardActionArea>
						
						<CardContent>
							
							<Typography
								gutterBottom
								variant="h6"
								component="h2"
							>
								
							You visited: {data.room_name} on

							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								from {data.start_date.slice(0, 10)} to {data.end_date.slice(0, 10)}
								<br></br>
								How was your experience?
							</Typography>
						</CardContent>
					</CardActionArea>
					Rate you experience
					<RadioGroup 
								
								style={{
									display: "flex",
									justifyContent: "center",
									alignItems: "center"
								  }}
								aria-label="rate"
								name="rate" 
								onChange={(event) => {
									this.setState({
										rating: event.target.value,
									});
								}}
							>
								<FormControlLabel
									value="1"
									variant="contained"
									control={<Radio />}
									label="1"
								/>
								<FormControlLabel
									value="2"
									control={<Radio />}
									label="2"
								/>
								<FormControlLabel
									value="3"
									control={<Radio />}
									label="3"
								/>
								<FormControlLabel
									value="4"
									control={<Radio />}
									label="4"
								/>
								<FormControlLabel
									value="5"
									control={<Radio />}
									label="5"
								/>
							</RadioGroup>
							<Button
					variant="contained"
					color="secondary"
					onClick={() => {
						 
							const rating = {
								user: decoded.userId,
								rating : this.state.rating,
								room : data.room,
							};
							axios
								.post(
									"https://localhost:3000/ratings",
									rating
								)
								.then((response) => {
									alert(response.data.message);
								})
								.catch((error) => {
									alert(error.response.data.message);
								});
						
					}}
				>
					Submit your rating
					
				</Button>
				<br></br>
				<br></br>

				</Card>
			</div>
		));

		return <div className="row">{markersListings}</div>;
	}
}
