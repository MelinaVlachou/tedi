import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import "../App.css";
import axios from "axios";
import { Link } from "react-router-dom";
import Cookie from "js-cookie";
import jwt_decode from "jwt-decode";


var token = Cookie.get("token");
if (token) {
	var decoded = jwt_decode(token);
	console.log(decoded);
}

export default class Listings extends Component {
	constructor(props) {
		super(props);

		this.state = {
			rooms: [],
		};
	}
	componentDidMount = () => {
		axios.get(`https://localhost:3000/rooms`).then((res) => {
			const listings = res.data.rooms;
			this.setState({ rooms: listings });
		});
	};

	render() {
		let markersListings = this.state.rooms
			.sort((a, b) => a.price - b.price)
			.map((data, index) => (
				<div
					style={{
						width: "80%",
						margin: "auto",
						marginBottom: "2rem",
					}}
					key={index}
				>
					<Card style={{ maxWidth: "345" }}>
						<CardActionArea>
							<CardMedia
								component="img"
								alt="Apartment image"
								height="345"
								image={data.image ? data.image : "logo512.png"}
								title="Apartment image"
								onClick={() => {
									window.location = `/rooms/${data._id}`;
								}}
							/>
							<CardContent>
								<Typography
									gutterBottom
									variant="h5"
									component="h2"
								>
									{data.name}
								</Typography>
								<Typography
									gutterBottom
									variant="h6"
									component="h2"
								>
									{data.location} - {data.price}€
								</Typography>
								<Typography
									variant="body2"
									color="textSecondary"
									component="p"
								>
									{data.description}
								</Typography>
							</CardContent>
						</CardActionArea>
						<CardActions>
							<Button
								size="small"
								color="primary"
								onClick={() => {
									if (token) {
										//take the room id
										const roomid = {
											roomid: data._id,
										};
										axios
											.patch(
												"https://localhost:3000/user/viewed/" +
													decoded.userId,
												roomid
											)
											.then((response) => {
												console.log(
													response.data.message
												);
											})
											.catch((error) => {
												console.log(
													error.response.data.message
												);
											});
									}
								}}
							>
								<Link
									to={{
										pathname: `/rooms/${data._id}`,
										idprop: data._id,
									}}
									style={{
										textDecoration: "none",
										textTransform: "capitalize",
									}}
								>
									View
								</Link>
							</Button>
						</CardActions>
					</Card>
				</div>
			));

		return <div className="row">{markersListings}</div>;
	}
}
