import "../App.css";
import Cookie from "js-cookie";
import React, { Component } from "react";
import { Input, Button } from "@material-ui/core";
import axios from "axios";
import Radio from "@material-ui/core/Radio";
import { BrowserRouter as  Link } from "react-router-dom";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import jwt_decode from "jwt-decode";
import { Map, Marker, TileLayer } from "react-leaflet";

var reader = new FileReader();
const position = [38.055, 23.8077];
var token = Cookie.get("token");
if (token) {
	var decoded = jwt_decode(token);
	console.log(decoded);
}

export default class Add_room extends Component {
	constructor(props) {
		super(props);
		this.state = {
			price: "",
			living_room: "",
			smoking: "",
			pets: "",
			gatherings: "",
			min_no_rentingdays: "",
			host_name: "",
			kitchen: "",
			no_people: "",
			no_bathrooms: "",
			no_beds: "",
			no_bedrooms: "",
			description: "",
			location: "",
			type: "",
			sqmeters: "",
			heating: "",
			floor: "",
			wifi: "",
			television: "",
			elevator: "",
			air_conditioning: "",
			parking: "",
			name: "",
			image: null,
		};
	}

	render() {
		return (
			<div>
				{!Cookie.get("token") ? (
					<>
						<h2>You are not authorized to view this page!</h2>{" "}
					</>
				) : (
					<>
						{!(
							decoded.role === "host" ||
							decoded.role === "tenderhost"
						) ? ( 
							<h2>You are not authorized to view this page!</h2>
						) : (<>
								{/* if verified */}
								{!decoded.verified ? (
									<>
										<h2>
											You are not verified as a host yet!
										</h2>{" "}
									</>
								) : (
									<>
										<h1 style={{ fontSize: "3rem" }}>
											<p className="shadow-box-example hoverable">
												Add your room information
											</p>
										</h1>

										<Map center={position} zoom={13}>
											<TileLayer
												url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
												attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
											/>
											<Marker position={position}>
												<link
													rel="stylesheet"
													href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.1/leaflet.css"
												></link>
											</Marker>
										</Map>
										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="text"
											label="Name"
											placeholder="Location"
											onChange={(event) => {
												this.setState({
													location:
														event.target.value,
												});
											}}
										/>

										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="text"
											placeholder="Description"
											onChange={(event) => {
												this.setState({
													description:
														event.target.value,
												});
											}}
										/>
										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="text"
											placeholder="Name"
											onChange={(event) => {
												this.setState({
													name: event.target.value,
												});
											}}
										/>
										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="number"
											label="sqmeters"
											placeholder="Sqmeters"
											onChange={(event) => {
												this.setState({
													sqmeters:
														event.target.value,
												});
											}}
										/>
										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="number"
											label="floor"
											placeholder="Floor"
											onChange={(event) => {
												this.setState({
													floor: event.target.value,
												});
											}}
										/>

										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="number"
											placeholder="Number of people"
											onChange={(event) => {
												this.setState({
													no_people:
														event.target.value,
												});
											}}
										/>
										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="number"
											placeholder="Price per day"
											onChange={(event) => {
												this.setState({
													price: event.target.value,
												});
											}}
										/>
										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="number"
											placeholder="Number of bathrooms"
											onChange={(event) => {
												this.setState({
													no_bathrooms:
														event.target.value,
												});
											}}
										/>

										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="number"
											placeholder="Number of Beds"
											onChange={(event) => {
												this.setState({
													no_beds: event.target.value,
												});
											}}
										/>

										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="number"
											placeholder="Number of Bedrooms"
											onChange={(event) => {
												this.setState({
													no_bedrooms:
														event.target.value,
												});
											}}
										/>

										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="number"
											placeholder="Minimum Rent Days"
											onChange={(event) => {
												this.setState({
													min_no_rentingdays:
														event.target.value,
												});
											}}
										/>

										<br />
										<br />
										<Input
											style={{
												backgroundColor: "white",
												borderRadius: 5,
											}}
											className="searchbar"
											type="text"
											placeholder="Host Name"
											onChange={(event) => {
												this.setState({
													host_name:
														event.target.value,
												});
											}}
										/>

										<br />
										<br />
										<img
											src={
												this.state.image
													? this.state.image
													: ""
											}
											alt="description "
											style={{
												width: "20rem",
												height: "20rem",
												display:
													this.state.image == null
														? "none"
														: "",
											}}
										/>
										<br />
										<Button
											variant="contained"
											component="label"
										>
											Upload Image
											<input
												id="upload-photo"
												type="file"
												style={{ display: "none" }}
												accept="image/png, image/jpeg"
												onChange={(event) => {
													reader.readAsDataURL(
														event.target.files[0]
													);
													reader.onloadend = () => {
														var base64data =
															reader.result;
														this.setState({
															image: base64data,
														});
													};
												}}
											/>
										</Button>
										<br />
										<br />

										<div>
											<form>
												<FormControl
													component="fieldset"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													<FormLabel
														component="legend"
														style={{
															backgroundColor:
																"white",
															borderRadius: 5,
														}}
													>
														Choose room type
													</FormLabel>

													<RadioGroup
														onChange={(event) => {
															this.setState({
																type:
																	event.target
																		.value,
															});
														}}
													>
														<FormControlLabel
															value="private room"
															variant="contained"
															control={<Radio />}
															label="private room"
														/>
														<FormControlLabel
															value="shared room"
															control={<Radio />}
															label="shared room"
														/>
														<FormControlLabel
															value="house"
															control={<Radio />}
															label="house"
														/>
													</RadioGroup>
												</FormControl>
											</form>
										</div>
										<br />
										<br />
										<div>
											<form >
												<FormControl
													component="fieldset"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													<FormLabel
														component="legend"
														style={{
															backgroundColor:
																"white",
															borderRadius: 5,
														}}
													>
														Heating
													</FormLabel>

													<RadioGroup
														onChange={(event) => {
															this.setState({
																heating:
																	event.target
																		.value,
															});
														}}
													>
														<FormControlLabel
															value="true"
															variant="contained"
															control={<Radio />}
															label="yes "
														/>
														<FormControlLabel
															value="false"
															control={<Radio />}
															label="no"
														/>
													</RadioGroup>
												</FormControl>
											</form>
										</div>
										<br />
										<br />
										<form >
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Wifi
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															wifi:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />

										<form>
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Television
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															television:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />

										<form >
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Elevator
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															elevator:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />
										<form>
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Air conditioning
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															air_conditioning:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />
										<form>
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Parking
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															parking:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />
										<form>
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Kitchen
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															kitchen:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />
										<form>
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Living room
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															living_room:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />
										<form>
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Smoking
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															smoking:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />
										<form>
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Pets
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															pets:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>

										<br />
										<br />
										<form>
											<FormControl
												component="fieldset"
												style={{
													backgroundColor: "white",
													borderRadius: 5,
												}}
											>
												<FormLabel
													component="legend"
													style={{
														backgroundColor:
															"white",
														borderRadius: 5,
													}}
												>
													Gatherings allowed
												</FormLabel>

												<RadioGroup
													onChange={(event) => {
														this.setState({
															gatherings:
																event.target
																	.value,
														});
													}}
												>
													<FormControlLabel
														value="true"
														variant="contained"
														control={<Radio />}
														label="yes "
													/>
													<FormControlLabel
														value="false"
														control={<Radio />}
														label="no"
													/>
												</RadioGroup>
											</FormControl>
										</form>
										<br />
										<br />

										<Button
											variant="contained"
											color="secondary"
											onClick={() => {
												if (1) {
													const user = {
														price: this.state.price,
														living_room: this.state
															.living_room,
														smoking: this.state
															.smoking,
														pets: this.state.pets,
														gatherings: this.state
															.gatherings,
														min_no_rentingdays: this
															.state
															.min_no_rentingdays,
														host_name: this.state
															.host_name,
														kitchen: this.state
															.kitchen,
														no_people: this.state
															.no_people,
														no_bathrooms: this.state
															.no_bathrooms,
														no_beds: this.state
															.no_beds,
														no_bedrooms: this.state
															.no_bedrooms,
														description: this.state
															.description,
														location: this.state
															.location,
														type: this.state.type,
														sqmeters: this.state
															.sqmeters,
														heating: this.state
															.heating,
														floor: this.state.floor,
														wifi: this.state.wifi,
														television: this.state
															.television,
														air_conditioning: this
															.state
															.air_conditioning,
														parking: this.state
															.parking,
														name: this.state.name,
														elevator: this.state
															.elevator,
														image: this.state.image,
													};
													axios
														.post(
															"https://localhost:3000/rooms",
															user
														)
														.then((response) => {
															alert(
																response.data
																	.message
															);
														})
														.catch((error) => {
															alert(
																error.response
																	.data
																	.message
															);
														});
												}
											}}
										>
											<Link
												to="/"
												style={{
													textDecoration: "none",
													textTransform: "capitalize",
												}}
											>
												Create Room
											</Link>
										</Button>
										<br />
										<br />
										<br />
									</>
								)}
							</>
						)}
					</>
				)}
				
			</div>
		);
	}
}
