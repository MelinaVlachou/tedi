const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const User = require("../models/user");
const checkAuth = require("../middleware/check-auth-admin");

router.post("/signup", (req, res, next) => {
	
	User.find({ username: req.body.username }) //check if username exists
		.exec()
		.then((user) => {
			if (user.length >= 1) {
				return res.status(409).json({
					message: "Username exists. Please insert new username. ",
				});
			} else {
				User.find({ email: req.body.email }) //check if email exists
					.exec()
					.then((user) => {
						if (user.length >= 1) {
							return res.status(409).json({
								message: "Email exists. Signup faild. ",
							});
						} else {
							bcrypt.hash(req.body.password, 10, (err, hash) => {
								if (err) {
									return res.status(500).json({
										error: err,
									});
								} else {
									const user = new User({
										_id: new mongoose.Types.ObjectId(),
										name: req.body.name,
										surname: req.body.surname,
										email: req.body.email,
										username: req.body.username,
										password: hash,
										verify_password: hash,
										phone: req.body.phone,
										city: req.body.city,
										role: req.body.role,
										rooms_viewed : [],
									});
									user.save()
										.then((result) => {
											console.log(result);
											
											res.status(201).json({
												message: "User created",
											});
										})
										.catch((err) => {
											console.log(err);
											res.status(500).json({
												error: err,
											});
										});
									if (req.body.role === "tenderhost") {
										res.status(201).json({
											message:
												"Waiting for approval from admin to login as a host, you can temporarily login as a tender",
										});
										console.log(
											"Waiting for approval from admin to login as a host, you can temporarily login as a tender"
										);
									}
									if (req.body.role === "host") {
										res.status(201).json({
											message:
												"Waiting for approval from admin to login as a host",
										});
										console.log(
											"Waiting for approval from admin to login as a host"
										);
									}
								}
							});
						}
					});
			}
		});
});

router.post("/login", (req, res, next) => {
	console.log(req.body);
	User.find({ email: req.body.email })
		.exec()
		.then((user) => {
			if (user.length < 1) {
				return res.status(200).json({
					message: "Email does not exist!",
				});
			}
			bcrypt.compare(
				req.body.password,
				user[0].password,
				(err, result) => {
					if (err) {
						return res.status(200).json({
							message: "Wrong password, please try again!",
						});
					}
					if (result) {
						const token = jwt.sign(
							{
								email: user[0].email,
								userId: user[0]._id,
								role: user[0].role,
								verified: user[0].verified,
								name: user[0].name,
								surname: user[0].surname,
								phone: user[0].phone,
								city: user[0].city,
								username: user[0].username,

							},
							process.env.JWT_KEY,
							{
								expiresIn: "1h",
							}
						);

						if (req.body.role === "host") {
							console.log(req.body.verified);
							if (req.body.verified) {
								///////////Here we must allow host rights
								return res.status(200).json({
									message: "Auth successful",
									token: token,
								});
							} else {
								return res.status(200).json({
									/////////////////Here we must not allow yet host rights
									message:
										"You can temporarily login only as a tender, until admin approves you as a host ",
									token: token,
								});
							}
						} else {
							res.cookie("token", token, { httpOnly: true });
							return res.status(200).json({
								message: "Auth successful",
								token: token,
							});
						}
					}
					res.status(200).json({
						message: "Wrong password, please try again!",
					});
				}
			);
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({
				error: err,
			});
		});
});


router.get("/approved", (req, res, next) => {
	var users = {
		role: "host",
		verified: "false",
	};

	User.find(users )
		.select("name email username surname city phone verified role rooms_viewed")
		.exec()
		.then((docs) => {
			if (docs) {
				
				console.log(docs);
				const respronse = {
				count: docs.length,
				user: docs.map((doc) => {
					return {
						name: doc.name,
						surname: doc.surname,
						email: doc.email,
						username: doc.username,
						phone: doc.phone,
						city: doc.city,
						role: doc.role,
						_id: doc._id,
						verified: doc.verified,
						rooms_viewed: doc.rooms_viewed,
					};
				}),
			};

			res.status(200).json(respronse);
			} else {
				res.status(404).json({
					message: "We did not find a non approved user",
				});
			}
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({ error: err });
		});
		
});

router.get("/approvedth", (req, res, next) => {
	var users = {
		role: "tenderhost",
		verified: "false",
	};

	User.find(users )
		.select("name email username surname city phone verified role rooms_viewed")
		.exec()
		.then((docs) => {
			if (docs) {
				
				console.log(docs);
				const respronse = {
				count: docs.length,
				user: docs.map((doc) => {
					return {
						name: doc.name,
						surname: doc.surname,
						email: doc.email,
						username: doc.username,
						phone: doc.phone,
						city: doc.city,
						role: doc.role,
						_id: doc._id,
						verified: doc.verified,
						rooms_viewed: doc.rooms_viewed,
					};
				}),
			};

			res.status(200).json(respronse);
			} else {
				res.status(404).json({
					message: "We did not find a non approved user",
				});
			}
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({ error: err });
		});
		
});

router.post("/username", (req, res, next) => {
	console.log(req.body.username);
	User.find({ username: req.body.username }) //check if username exists
		.select("_id")
		.exec()
		.then((docs) => {
			if (docs.length >= 1) {
				console.log(docs);
				const respronse = {
					//count: docs.length,
					user: docs.map((doc) => {
						return {
							_id: doc._id
						};
					}),
				};
				res.status(200).json(respronse);
			}
			
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({ error: err });
		});
	});

router.get("/", (req, res, next) => {
	User.find()
		.select("name email username surname city phone verified role rooms_viewed ")
		.exec()
		.then((docs) => {
			console.log(docs);
			const response = {
				count: docs.length,
				user: docs.map((doc) => {
					return {
						name: doc.name,
						surname: doc.surname,
						email: doc.email,
						username: doc.username,
						phone: doc.phone,
						city: doc.city,
						role: doc.role,
						_id: doc._id,
						verified: doc.verified,
						rooms_viewed : doc.rooms_viewed,
					};
				}),
			};

			res.status(200).json(response);
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({ error: err });
		});
});


//patch to push a new room into viewed rooms
router.patch("/viewed/:userId", (req, res, next) => {
	const id = req.params.userId;
	//const updateOps = {};
	
	User.update({ _id: id }, { $push: { rooms_viewed: req.body.roomid } })
		.exec()
		.then((result) => {
			res.status(200).json({
				message: "User updated",
				request: {
					type: "GET",
					url: "http://localhost:3000/user/" + id,
				},
			});
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({
				error: err,
			});
		});
});

router.delete(
	"/:userId",
	/*checkAuth,*/ (req, res, next) => {
		User.remove({ _id: req.params.userId })
			.exec()
			.then((result) => {
				res.status(200).json({
					message: "User deleted",
				});
			})
			.catch((err) => {
				console.log(err);
				res.status(500).json({
					error: err,
				});
			});
	}
);



router.patch("/:userId", (req, res, next) => {
	const id = req.params.userId;
	const updateOps = {};
	for (const ops of req.body) {
		updateOps[ops.propName] = ops.value;
	}
	User.update({ _id: id }, { $set: updateOps })
		.exec()
		.then((result) => {
			res.status(200).json({
				message: "User updated",
				request: {
					type: "GET",
					url: "http://localhost:3000/user/" + id,
				},
			});
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({
				error: err,
			});
		});
});




// router.get("/:userId", (req, res, next) => {
// 	const id = req.params.userId;
// 	User.findById(id)
// 		.select("rooms_viewed ")
// 		.exec()
// 		.then((docs) => {
// 			console.log(docs);
// 			const response = {
// 				rooms_viewed : docs.rooms_viewed
				
// 			};
			
// 			res.status(200).json(response);
// 		})
// 		.catch((err) => {
// 			console.log(err);
// 			res.status(500).json({ error: err });
// 		});
// });
module.exports = router;
