const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const multer = require("multer");


const Booking = require("../models/booking");

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, "./uploads/");
	},
	filename: function (req, file, cb) {
		cb(null, new Date().toISOString() + file.originalname);
	},
});

const fileFilter = (req, file, cb) => {
	//reject a file
	if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
		cb(null, true);
	} else {
		cb(null, false);
	}
};

const upload = multer({
	storage: storage,
	limits: {
		filesize: 1024 * 1024 * 5,
	},
	fileFilter: fileFilter,
});

const Room = require("../models/room");

function dateRangeOverlaps(a_start, a_end, b_start, b_end) {
	if (a_start <= b_start && b_start <= a_end) return true;
	if (a_start <= b_end && b_end <= a_end) return true;
	if (b_start < a_start && a_end < b_end) return true;
	return false;
}

router.get("/", (req, res, next) => {
	Room.find()
		.exec()
		.then((docs) => {
			const response = {
				count: docs.length,
				rooms: docs.map((doc) => {
					return {
						name: doc.name,
						price: doc.price,
						no_people: doc.no_people,
						elevator: doc.elevator,
						kitchen: doc.kitchen,
						floor: doc.floor,
						room_image: doc.room_image,
						location: doc.location,
						type: doc.type,
						sqmeters: doc.sqmeters,
						heating: doc.heating,
						air_conditioning: doc.air_conditioning,
						wifi: doc.wifi,
						parking: doc.parking,
						television: doc.television,
						no_bathrooms: doc.no_bathrooms,
						no_beds: doc.no_beds,
						no_bedrooms: doc.no_bedrooms,
						living_room: doc.living_room,
						description: doc.description,
						smoking: doc.smoking,
						pets: doc.pets,
						gatherings: doc.gatherings,
						min_no_rentingdays: doc.min_no_rentingdays,
						host_name: doc.host_name,
						host_reviews: doc.host_reviews,
						host_image: doc.host_image,
						_id: doc._id,
						image: doc.image,
					};
				}),
			};
			res.status(200).json(response);
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({ error: err });
		});
});

router.post("/search", (req, res, next) => {
	var sDate = new Date(req.body.start_date);
	var eDate = new Date(req.body.end_date);
	if (sDate > eDate) {
		return res.status(200).json({
			message: "Start date greater than end date",
		});
	} else if (sDate < new Date()) {
		res.status(401).json({
			message: "Past date",
		});
	} else {
		Room.find(req.body.room)
			.exec()
			.then((docs) => {
				const response = {
					count: docs.length,
					rooms: docs.map((doc) => {
						return {
							name: doc.name,
							price: doc.price,
							no_people: doc.no_people,
							elevator: doc.elevator,
							kitchen: doc.kitchen,
							floor: doc.floor,
							room_image: doc.room_image,
							location: doc.location,
							type: doc.type,
							sqmeters: doc.sqmeters,
							heating: doc.heating,
							air_conditioning: doc.air_conditioning,
							wifi: doc.wifi,
							parking: doc.parking,
							television: doc.television,
							no_bathrooms: doc.no_bathrooms,
							no_beds: doc.no_beds,
							no_bedrooms: doc.no_bedrooms,
							living_room: doc.living_room,
							description: doc.description,
							smoking: doc.smoking,
							pets: doc.pets,
							gatherings: doc.gatherings,
							min_no_rentingdays: doc.min_no_rentingdays,
							host_name: doc.host_name,
							host_reviews: doc.host_reviews,
							host_image: doc.host_image,
							image: doc.image,
							_id: doc._id,
							request: {
								type: "GET",
								url: "http://localhost:3000/rooms/" + doc._id,
							},
						};
					}),
				};
				var obj = [];
				const len = response.rooms.length - 1;
				response.rooms.map((room, index) => {
					var sDate = new Date(req.body.start_date);
					var eDate = new Date(req.body.end_date);
					var roomId = room._id;
					var available = true;
					var isLast = index === len;
					Booking.find({ room: roomId })
						.select("_id room start_date end_date")
						.populate("room", "name")
						.exec()
						.then((docs) => {
							if (sDate > eDate) {
								return (available = false);
							} else if (sDate < new Date()) {
								return (available = false);
							} else {
								for (var i = 0; i < docs.length; i++) {
									if (
										dateRangeOverlaps(
											new Date(docs[i].start_date),
											new Date(docs[i].end_date),
											sDate,
											eDate
										)
									) {
										return (available = false);
									}
								}
							}
							return (available = true);
						})
						.then((available) => {
							if (available) {
								obj.push(room);
							}
							if (isLast) {
								res.status(200).json({
									count: obj.length,
									rooms: obj,
								});
							}
						})
						.catch((err) => {
							res.status(500).json({
								error: err.message,
							});
						});
				});
			})
			.catch((err) => {
				console.log(err);
				res.status(500).json({ error: err });
			});
	}
});

router.post(
	"/" ,
	upload.single("room_image"),
	(req, res, next) => {
		const room = new Room({
			_id: new mongoose.Types.ObjectId(),
			name: req.body.name,
			price: req.body.price,
			location: req.body.location,
			type: req.body.type,
			sqmeters: req.body.sqmeters,
			heating: req.body.heating,
			floor: req.body.floor,
			wifi: req.body.wifi,
			television: req.body.television,
			elevator: req.body.elevator,
			air_conditioning: req.body.air_conditioning,
			parking: req.body.parking,
			kitchen: req.body.kitchen,
			no_people: req.body.no_people,
			no_bathrooms: req.body.no_bathrooms,
			no_beds: req.body.no_beds,
			no_bedrooms: req.body.no_bedrooms,
			living_room: req.body.living_room,
			description: req.body.description,
			smoking: req.body.smoking,
			pets: req.body.pets,
			gatherings: req.body.gatherings,
			min_no_rentingdays: req.body.min_no_rentingdays,
			host_name: req.body.host_name,
			host_reviews: req.body.host_reviews,
			image: req.body.image,
		});
		room.save()
			.then((result) => {
				console.log(result);
				res.status(201).json({
					message: "Created room successfully",
					createdRoom: {
						name: result.name,
						price: result.price,
						_id: result._id,
						request: {
							type: "GET",
							url: "http://localhost:3000/rooms/" + result._id,
						},
					},
				});
			})
			.catch((err) => {
				console.log(err);
				res.status(500).json({ error: err });
			});
	}
);


router.get("/:roomId", (req, res, next) => {
	const id = req.params.roomId;
	Room.findById(id)
		.exec()
		.then((doc) => {
			console.log("From database", doc);
			if (doc) {
				res.status(200).json({
					room: doc,
					request: {
						type: "GET",
						url: "http://localhost:3000/rooms",
					},
				});
			} else {
				res.status(404).json({
					message: "No valid entry found for provided id",
				});
			}
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({ error: err });
		});
});

router.patch("/:roomId", (req, res, next) => {
	const id = req.params.roomId;
	const updateOps = {};
	for (const ops of req.body) {
		updateOps[ops.propName] = ops.value;
	}
	Room.update({ _id: id }, { $set: updateOps })
		.exec()
		.then((result) => {
			res.status(200).json({
				message: "Room info updated!",
				request: {
					type: "GET",
					url: "http://localhost:3000/rooms/" + id,
				},
			});
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({
				error: err,
			});
		});
});

router.delete("/:roomId", (req, res, next) => {
	const id = req.params.roomId;
	console.log(id);
	Room.remove({ _id: id })
		.exec()
		.then((result) => {
			res.status(200).json({
				message: "Room deleted",
				request: {
					type: "GET",
					url: "http://localhost:3000/rooms",
					body: { name: "String", price: "Number" },
				},
			});
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({
				error: err,
			});
		});
});

router.post("/host_name", (req, res, next) => {
	Room.find({ host_name: req.body.host_name })
		.exec()
		.then((docs) => {
			if (docs.length >= 1) {
				console.log(docs);
				const respronse = {
					room: docs.map((doc) => {
						return {
							name: doc.name,
							price: doc.price,
							no_people: doc.no_people,
							elevator: doc.elevator,
							kitchen: doc.kitchen,
							floor: doc.floor,
							room_image: doc.room_image,
							location: doc.location,
							type: doc.type,
							sqmeters: doc.sqmeters,
							heating: doc.heating,
							air_conditioning: doc.air_conditioning,
							wifi: doc.wifi,
							parking: doc.parking,
							television: doc.television,
							no_bathrooms: doc.no_bathrooms,
							no_beds: doc.no_beds,
							no_bedrooms: doc.no_bedrooms,
							living_room: doc.living_room,
							description: doc.description,
							smoking: doc.smoking,
							pets: doc.pets,
							gatherings: doc.gatherings,
							min_no_rentingdays: doc.min_no_rentingdays,
							host_name: doc.host_name,
							host_reviews: doc.host_reviews,
							_id: doc._id,
						};
					}),
				};
				res.status(200).json(respronse);
			}
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({ error: err });
		});
});

module.exports = router;
