const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const checkAuth = require("../middleware/check-auth");

const Rating = require("../models/rating");
const Room = require("../models/room");
const User = require("../models/user");

//handle incoming GET requests to /Booking
router.get("/", (req, res, next) => {
	Rating.find()
		//.select("room _id rating user")
		//.populate("room", "user ")
		.exec()
		.then((docs) => {
			res.status(200).json({
				count: docs.length,
				ratings: docs.map((doc) => {
					return {
						_id: doc._id,
						room: doc.room,
                        user: doc.user,
                        rating: doc.rating,
						request: {
							type: "GET",
							url: "http://localhost:3000/ratings/" + doc._id,
						},
					};
				}),
			});
		})
		.catch((err) => {
			res.status(500).json({
				error: err,
			});
		});
});

router.post(
	"/" ,
	(req, res, next) => {
		console.log(req.body);
		const rating = new Rating({
            _id: new mongoose.Types.ObjectId(),
            room: req.body.room,
            user: req.body.user,
            rating: req.body.rating,
		});
		rating.save()
			.then((result) => {
				console.log(result);
				res.status(201).json({
					message: "rating posted successfully",
					createdRating: {
						room: result.room,
						user: result.user,
                        _id: result._id,
                        rating: result.rating,
						request: {
							type: "GET",
							url: "http://localhost:3000/ratings/" + result._id,
						},
					},
				});
			})
			.catch((err) => {
				console.log(err);
				res.status(500).json({ error: err });
			});
	}
);


router.get("/:userId", (req, res, next) => {
	
	Rating.find({ user: req.params.userId })
		.exec()
		.then((docs) => {
			if (docs) {
				console.log(req.params.userId);
				console.log(docs);
				const respronse = {
				count: docs.length,
				user: docs.map((doc) => {
					return {
						room_id: doc._id,
						room: doc.room,
						user: doc.user,
						rating: doc.rating,
					};
				}),
			};

			res.status(200).json(respronse);
			} else {
				res.status(200).json({
					message: "We did not find a rating from this user",
				});
			}
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({ error: err });
		});
});

module.exports = router;
