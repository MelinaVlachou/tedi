const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const checkAuth = require("../middleware/check-auth");

const Booking = require("../models/booking");
const Room = require("../models/room");

//handle incoming GET requests to /Booking
router.get("/", (req, res, next) => {
	Booking.find()
		.select("room quantity _id reservation_name start_date end_date room_name")
		.populate("room", "name ")
		.exec()
		.then((docs) => {
			res.status(200).json({
				count: docs.length,
				bookings: docs.map((doc) => {
					return {
						_id: doc._id,
						room: doc.room,
						start_date: doc.start_date,
						end_date: doc.end_date,
						reservation_name: doc.reservation_name,
						tel: doc.tel,
						room_name: doc.room_name,
						request: {
							type: "GET",
							url: "http://localhost:3000/bookings/" + doc._id,
						},
					};
				}),
			});
		})
		.catch((err) => {
			res.status(500).json({
				error: err,
			});
		});
});

function dateRangeOverlaps(a_start, a_end, b_start, b_end) {
	if (a_start <= b_start && b_start <= a_end) return true;
	if (a_start <= b_end && b_end <= a_end) return true;
	if (b_start < a_start && a_end < b_end) return true;
	return false;
}

router.post("/", (req, res, next) => {
	/* Check if room is available */
	var sDate = new Date(req.body.start_date);
	var eDate = new Date(req.body.end_date);
	var roomId = req.body.room;
	Booking.find({ room: roomId })
		.select("_id room start_date end_date")
		.populate("room", "name")
		.exec()
		.then((docs) => {
			if (sDate > eDate) {
				throw new Error("Start date greater than end date");
			} else if (sDate < new Date()) {
				throw new Error("Past date");
			} else {
				docs.map((doc) => {
					if (
						dateRangeOverlaps(
							new Date(doc.start_date),
							new Date(doc.end_date),
							sDate,
							eDate
						)
					) {
						throw new Error("Room not available");
					}
				});
			}
			/* Save booking */
			const booking = new Booking({
				_id: mongoose.Types.ObjectId(),
				start_date: sDate,
				end_date: eDate,
				reservation_name: req.body.reservation_name,
				tel: req.body.tel,
				room: roomId,
				room_name: req.body.room_name
			});
			console.log(booking);
			return booking
				.save()
				.then((result) => {
					//console.log(result);
					res.status(201).json({
						message: "Booking stored",
						createdBooking: {
							_id: result._id,
							room: result.room,
							start_date: result.start_date,
							end_date: result.end_date,
							reservation_name: result.reservation_name,
							tel: result.tel,
						},
						request: {
							type: "GET",
							url: "http://localhost:3000/bookings/" + result._id,
						},
					});
				})
				.catch((err) => {
					//console.log(err);
					res.status(500).json({
						error: err,
					});
				});
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({
				error: err.message,
			});
		});
});

router.get("/:bookingId", (req, res, next) => {
	Booking.findById(req.params.bookingId)
		.populate("room")
		.exec()
		.then((booking) => {
			if (!booking) {
				return res.status(404).json({
					message: "Booking not found",
				});
			}
			res.status(200).json({
				booking: booking,
				request: {
					type: "GET",
					url: "http://localhost:3000/bookings",
				},
			});
		})
		.catch((err) => {
			res.status(500).json({
				error: err,
			});
		});
});

router.delete("/:bookingId", (req, res, next) => {
	Booking.remove({ _id: req.params.bookingId })
		.exec()
		.then((result) => {
			res.status(200).json({
				message: "Booking deleted",
				request: {
					type: "POST",
					url: "http://localhost:3000/bookings",
					body: { roomId: "ID" },
				},
			});
		})
		.catch((err) => {
			res.status(500).json({
				error: err,
			});
		});
});

router.delete("/", (req, res, next) => {
	Booking.find()
		.select("_id ")
		.exec()
		.then((docs) => {
			docs.map((doc) => {
				console.log(doc._id);
				Booking.remove({ _id: doc._id });
			});
		})
		.then((result) => {
			res.status(200).json({
				message: "Bookings deleted",
			});
		})
		.catch((err) => {
			res.status(500).json({
				error: err,
			});
		});
});

router.get("/room/:roomId", (req, res, next) => {
	Booking.find({ room: req.params.roomId })
		.select("_id room start_date end_date")
		.populate("room", "name")
		.exec()
		.then((docs) => {
			res.status(200).json({
				count: docs.length,
				bookings: docs.map((doc) => {
					return {
						_id: doc._id,
						room: doc.room,
						start_date: doc.start_date,
						end_date: doc.end_date,
						reservation_name: doc.reservation_name,
						tel: doc.tel,
						request: {
							type: "GET",
							url: "http://localhost:3000/bookings/" + doc._id,
						},
					};
				}),
			});
		})
		.catch((err) => {
			res.status(500).json({
				error: err,
			});
		});
});
router.get("/username/:username", (req, res, next) => {
	Booking.find({ reservation_name: req.params.username })
		.select("_id room start_date end_date room_name")
		//.populate("room", "name")
		.exec()
		.then((docs) => {
			// res.status(200).json({
			// 	count: docs.length,
			// 	bookings: docs.map((doc) => {
			// 		return {
			// 			// _id: doc._id,
			// 			// room: doc.room,
			// 			// start_date: doc.start_date,
			// 			// end_date: doc.end_date,
			// 			// reservation_name: doc.reservation_name,
			// 			// tel: doc.tel,
			// 			request: {
			// 				type: "GET",
			// 				url: "http://localhost:3000/bookings/" + doc._id,
			// 			},
			// 		};
			// 	}),
			// });
			if (docs) {
				
				console.log(docs);
				const respronse = {
				count: docs.length,
				bookings: docs.map((doc) => {
					return {
						_id: doc._id,
						room: doc.room,
						start_date: doc.start_date,
			 			end_date: doc.end_date,
						reservation_name: doc.reservation_name,
						tel: doc.tel,
						room_name: doc.room_name,
						request: {
											type: "GET",
										url: "http://localhost:3000/bookings/" + doc._id,
									},
					};
				}),
			};

			res.status(200).json(respronse);
			} else {
				res.status(404).json({
					message: "We did not find a non approved user",
				});
			}
		})
		.catch((err) => {
			res.status(500).json({
				error: err,
			});
		});
});

module.exports = router;
