const express = require("express");
const router = express.Router();
const User = require("../models/user");
const Room = require("../models/room");



router.get("/:userId", (req, res, next) => {
	const id = req.params.userId;
	noRating(id)
		.then((array) => {
			const response = { recommendations: array };
			res.status(200).json(response);
		})
		.catch((e) => {
			res.status(401).json(e);
		});
});

async function noRating(id)
{
	var docs = [];
	var response = [Room];
	docs = await User.findById(id).exec();
	let position= 0;
	//300 is too much and it gets the job done
	for (var i = 0; i < 300; i++)
	{
		
		if(docs.rooms_viewed[i]!= null && docs.rooms_viewed[i] != undefined)
		{

			response[position] = await Room.findById(docs.rooms_viewed[i]);
			//console.log(docs.rooms_viewed[i]);
			position++;
		}
		
		
	}
	
	console.log("coreecttt",response);
	return response;


}

module.exports = router;