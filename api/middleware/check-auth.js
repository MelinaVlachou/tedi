const jwt = require('jsonwebtoken');
module.exports = (req, res, next) => {


    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        req.userData = decoded;
        console.log(req.userData.role);
        if(req.userData.role === "host")
        {next();}
        else 
        {console.log("You have no rights to access here");
        return res.status(401).json({
            message: 'Auth failed'});}
    } catch (error) {
        return res.status(401).json({
            message: 'Auth failed'
        });
    }
};
