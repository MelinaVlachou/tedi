const mongoose = require('mongoose');

const bookingSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    room: {type: mongoose.Schema.Types.ObjectId, ref: 'Room' , required: true}, //connect this schema with room(building a relationship even though modoDB is non-relational)
    start_date: { type: Date, required: true },
    end_date: { type: Date, required: true },
    reservation_name: { type: String, required: true },
    tel: { type: String, required: true },
    room_name : { type: String, required: true }


});

module.exports = mongoose.model('Booking', bookingSchema);
