const mongoose = require('mongoose');

const ratingSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    room: {type: mongoose.Schema.Types.ObjectId, ref: 'Room' , required: true}, 
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User' , required: true},
    rating: { type: Number, required: true, min: 1, max: 5}
});

module.exports = mongoose.model('Rating', ratingSchema);
