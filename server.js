// password = password
const https = require("https");
const app = require("./app");
const fs = require("fs");
const port = process.env.PORT || 3000;

const options = {
	key: fs.readFileSync("key.pem"),
	cert: fs.readFileSync("cert.pem"),
};

const server = https.createServer(options, app);

server.timeout = 60000;
server.listen(port);
