const express = require("express");
const app = express();
const morgan = require("morgan"); //morgan is the "next", prints the request in terminal
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
var cors = require("cors");

const roomRoutes = require("./api/routes/rooms");
const bookingRoutes = require("./api/routes/bookings");
const userRoutes = require("./api/routes/user");
const ratingRoutes = require("./api/routes/ratings");
const recommendationsRoutes = require("./api/routes/matrixfact");
const noRating = require("./api/routes/noRating");

mongoose.connect(
	"mongodb+srv://tedi2020:" +
		process.env.MONGO_ATLAS_PW +
		"@rest-api-o2nzu.mongodb.net/test?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);
mongoose.Promise = global.Promise;
app.use(cors());
app.options("*", cors());
app.use(express.static("public"));

app.use(morgan("dev"));
app.use("/uploads", express.static("uploads"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(
	bodyParser.urlencoded({
		limit: "50mb",
		extended: true,
		parameterLimit: 50000,
	})
);

/*
app.use(express.static("public"));
app.use(express.json());
*/
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Header",
		"Origin , X-Requested-With, Content-Type, Accept, Authorization"
	);
	if (req.method == "OPTIONS") {
		res.header("Access-COntrol-Allow-Methods", "PUT,POST,PATCH,DELETE,GET");
		return res.status(200).json({});
	}
	next();
});

//routes which should handle requests
app.use("/rooms", roomRoutes);
app.use("/bookings", bookingRoutes);
app.use("/user", userRoutes);
app.use("/ratings", ratingRoutes);
app.use("/matrixfact", recommendationsRoutes);
app.use("/noRating", noRating);

app.use((req, res, next) => {
	//in case none of the routes bookings, rooms could handle a request:

	const error = new Error("Not found");
	error.status = 404;
	next(error); //pass error along with the next function: forward the error request
});

app.use((error, req, res, next) => {
	//to handle any error unlike the above
	res.status(error.status || 500);
	res.json({
		error: {
			message: error.message,
		},
	});
});

module.exports = app;
